Past Continuous (was/were Ving)

- Действие или ряд действий длились в какой то момент времени в прошлом.

Обычно применяется в рассказах о длительных действиях в прошлом.

Момент действия в прошлом должен быть указан (в отличие от PastSimple, когда говорим просто о факте свершившегося действия в прошлом):
- конкретное время (at 5 o'clock);
- указание на момент времени (when). When she called I was taking a shower.

- одновременные действия в прошлом. We were eating pizza while we were watching a new TV series. (Мы ели пиццу, когда смотрели новый сериал по ТВ)
- неодобрение. (often, always, constantly). He was always losing our keys while on holiday. (Он постоянно терял наши ключи на отдыхе)
- временная ситуация. Обязательно указывается конкретный промежуток. They were living in Norway for 4 months. (Они жили в Норвегии 4 месяца).

Если действия однократные и происходят друг за другом, то PastSimple: I woke up and  opened the door. (Я проснулся и открыл дверь)
Если действия происходили одновременно (параллельно) то PastContinuous: Tommy was playing a video game while I was doing my homework. (Томми играл в видеоигру, пока я делал домашнюю работу)

Маркеры: at the moment, when, while, as, all night (long), all morning, all day,  last Sunday,  last month, last year, at 6 yesterday.